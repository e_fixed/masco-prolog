Masco-Prolog 是一门纯逻辑语言，不支持数学运算。

（虽然Fixed告诉我还没写好，但是wiki还是要写的）

使用Masco-Prolog写代码时，代码中的空格，换行，\r，\t都会被视作为换行，并以此来分割每行代码。

以demo为例，展示Masco-Prolog目前的功能：
```prolog
human(sb): {
(+has_eye(sb)),
(-has_fur(sb)),
}.
[+human(Fixed)].
[+human(Lord)].
```
这段代码先定义了一个关系：

human(sb):{(+has_eye(sb)),(-has_fur(sb)),}.

这句话中包含数个部分，表示“human()”这个关系，
当sb有has_eye关系且有 非has_fur 关系时human(sb)也成立，
或者sb有human关系时 他也有has_eye关系和非has_fur关系

然后给变量添加一个逻辑：

[+human(Fixed)].

代表Fixed有human逻辑，如果把“+”变为“-”即：

[-human(Fixed)].

则表示Fixed有非human逻辑，同时human关系的子逻辑全部不成立。

demo代码的运行结果是
Fixed有human关系，且Lord有human关系。

检验方法是在命令行中写下human.

目前项目仍在编写，不完善请见谅。

by lyl123&Fixed
2024/1/29