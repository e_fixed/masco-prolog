# Masco-Prolog READ.ME
<img src="Masco&prolog.jpg"></img> <h3>Masco-Prolog 是一门纯逻辑型语言，常用于逻辑分析、构建专家系统。</h3>
Fixed和Lord同为人类论证：
```prolog
human(sb): {
  (+has_eye(sb)),
  (-has_fur(sb)),
}.
[+human(Fixed)].
[+human(Lord)].
```
<h5> Masco-Prolog获得 </h5>
注: 目前Masco-Prolog还未正式开发完毕，但您可以这样做，以体验Masco-Prolog develop!

1. 单击【克隆/下载▾】按钮
2. 选择【下载ZIP】或者复制代码 https://gitee.com/classzheng/masco-prolog.git
3. 打开Terminal，并输入：
```shell
git clone https://gitee.com/classzheng/masco-prolog.git
```

<h5> 使用Masco-Prolog </h5>
注：目前Masco-Prolog还未开发完毕，但您可以这样做，来使用Masco-Prolog develop的部分功能！

1. 打开【克隆】下来的代码压缩包，并且找到【cpp文件夹】或是【objc文件夹】
2. 打开文件夹，用【集成开发环境】打开目录中的mascopl.cpp或者mascopl.m
3. 将您的MascProlog代码输入到demo.aline/demo.Aline中
4. 自行编译，如果编译失败请检查您的编译器是否达到C++11标准或是ObjC v2标准

注：详细内容请移步Wiki!

<h5> Masco-Prolog的编译参数 </h5>
注：目前Masco-Prolog还未开发完毕，但您仍然可以学习Masco-Prolog！

Masco-Prolog有三个命令行参数，分别为：
 .cmd_mode 是否启用命令行查询模式
 .no_info  是否提供额外的帮助信息
 .halt_cmd 是否在命令行提供halt命令

<h5> Masco-Prolog的捐赠 </h5>
如果您想要帮助我们的开发，

请将其捐赠给开发人员lyl123，并且感谢您的支持！