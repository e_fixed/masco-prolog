// Masco.h
@interface Masco : NSObject
 {
  @public
	NSString*Label;
	NSString*Aline;
	struct {
		BOOL cmd_mode;
		BOOL no_info;
		BOOL halt_cmd;
	} Option;
	int NexusDtbs;
	int EpilogDtbs;
 }
-(void) split;
-(void) com2cmd;
@end