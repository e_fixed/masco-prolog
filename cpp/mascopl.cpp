#include <iostream>
#include <string>
#include <vector>
#define when(tr1,todo) if(tr1) todo
struct Masco {
    std::string label;
	std::string aline;
	struct {
		bool cmd_mode;
		bool no_info;
		bool halt_cmd;
	} option;
	std::vector<std::string> nexus_dtbs;
	std::vector<Masco> epilog_dtbs;
    std::vector<std::string> object;
	void split();
	void com2cmd();
};
void Masco::split() {
	for(auto&ch:aline) {
		when(ch==' ',ch='/');
		when(ch=='\n',ch='/');
		when(ch=='\r',ch='/');
		when(ch=='\t',ch='/');
	}
	while(aline.find("/")!=-1) aline.erase(aline.find("/"),1);
	while (true) {
        size_t start = aline.find("[");
        size_t end = aline.find("]");
        if (start == -1 || end == -1) {
            break;
        }
        if (start < end) {
            nexus_dtbs.push_back(aline.substr(start, end - start + 1)+".");
            aline.erase(start, end - start + 2);
        } else {
            break;
        }
    }
    if(label.empty())
    for(auto is:nexus_dtbs) {
        is.erase(is.length()-3,3);
        is.erase(0,is.find("(")+1);
        object.push_back(is);
    }
    for(auto&is:object) {
        std::vector<std::string> nexus;
        for(auto&js:nexus_dtbs) {
            if(js.find(is)!=-1){
                js.erase(js.find("(")+1);
                js.erase(0,1);
                nexus.push_back(js);
            }
        }
        for(auto&js:epilog_dtbs) {
            for(auto ks:js.nexus_dtbs) {
                ks.erase(0,1);
                ks.erase(ks.find("(")+1);
                int suc=0;
                for(auto&ls:nexus) {
                    if(ls==ks) suc++;
                }
                if()
            }
        }
    }
    if(aline.empty()) return;
    while(!aline.empty()) {
        size_t dotPos = aline.find(".");
        if(dotPos==-1) break;
        Masco push;
        push.aline=aline.substr(0,dotPos-1);
        push.label=push.aline.substr(0,push.aline.find(":")+1);
        push.aline.erase(0,push.aline.find(":")+2);
        for(int i = 0; i < push.aline.length()-1; i++) {
            if(push.aline[i]=='('&&(push.aline[i+1]=='+'||push.aline[i+1]=='-')){push.aline[i]='[';continue;}
            if(push.aline[i]==')'&&push.aline[i-1]==')') push.aline[i]=']';
        }
        push.split();
        epilog_dtbs.push_back(push);
        aline=aline.substr(dotPos+1);
    }
	return;
}
void Masco::com2cmd() {
    if(!option.cmd_mode) return;
    while(1) {
        std::cout << "?- ";
        std::string cmd;
        std::cin>>cmd;
        if(cmd=="halt."&&option.halt_cmd) break;
        if(cmd[0]!='[') {
            if(cmd[cmd.length()-1]!='.'&&(!option.no_info)) {
                std::cout << "[Warning] Incorrect expression.\n";
            }
            cmd.erase(cmd.length()-1);
            for(auto&nexus:nexus_dtbs) {
                if(nexus.find(cmd)!=-1) {
                    std::cout << nexus << "\n";
                }
            }
            continue;
        }
        bool found=false;
        for(auto&nexus:nexus_dtbs) {
            if(nexus==cmd) {
                std::cout << "true.\n";
                found=true;
                break;
            }
        }
        if(!found) std::cout << "false.\n";
    }
    return;
}
int main(int argc,char*argv[]) {
	Masco demo={
		.aline=R"(
			human(sb): {
			  (+has_eye(sb)),
			  (-has_fur(sb)),
			}.
			[+human(Fixed)].
			[+human(Lord)].
		)",
		.option={
		    .cmd_mode=true,
		    .no_info=false,
		    .halt_cmd=true,
		},
	};
	demo.split();
	demo.com2cmd();
	return 0;
}